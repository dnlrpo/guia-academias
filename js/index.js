$(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('.carousel').carousel({
    interval: 2000
  });
  $('#dejarDatos').on('show.bs.modal',function(e){
    console.log('el modal se está mostrando');
    $('.btn-info-academia').removeClass("btn-primary");
    $('.btn-info-academia').addClass("btn-default");
    $('.btn-info-academia').prop('disabled',true);
  })
  $('#dejarDatos').on('show.bs.modal',function(e){
    console.log('el modal se mostró');
  })
  $('#dejarDatos').on('hide.bs.modal',function(e){
    console.log('el modal se está cerrando');
    $('.btn-info-academia').removeClass("btn-default");
    $('.btn-info-academia').addClass("btn-primary");
    $('.btn-info-academia').prop('disabled',false);
  })
  $('#dejarDatos').on('hide.bs.modal',function(e){
    console.log('el modal se cerró');
  })
});
